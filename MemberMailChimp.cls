/**********************************************************************
* Name:     MemberMailChimp
* Author:   BFL
*  
* ======================================================
* ======================================================
* Purpose:  Get members from Mailchamp.
* ======================================================
* ======================================================
* History:                                                            
* VERSION   DATE            INITIALS      DESCRIPTION/FEATURES ADDED
* 1.0       16-Apr-2018     BFL           Get members from Mailchamp.           
*   
***********************************************************************/

public class MemberMailChimp {
    
    private static Integer getRemainingCallout() {
        // callout limits 
        return Limits.getLimitCallouts() - Limits.getCallouts(); 
    }
    
    // Geting all members. 
    public static void getMailChimpMembers() {
        // custom setting. 
        MemberMailChimpCS__c memberMailChimpSetting = MemberMailChimpCS__c.getOrgDefaults();
        
        string endpoint =memberMailChimpSetting.endpoint__c;
        string username =memberMailChimpSetting.Username__c; 
        string password =memberMailChimpSetting.Password__c;
        string method = 'GET';
        
        MemberWrapper mailChimpList = new MemberWrapper();
        List<Member__c> memberList = new List<Member__c>();
        
        if(getRemainingCallout() > 0) {
            
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http h = new Http();
            
            req.setEndpoint(endpoint);
            req.setMethod(method);
            req.setTimeout(120000);
            
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'application/json');
            
            try{
                res = h.send(req);
                system.debug('res--->'+res);
            }
            catch(Exception e){
                System.debug('Error in callout |'+e.getMessage());
            }
            
            if(res.getStatusCode() == 200){
                mailChimpList = (MemberWrapper)JSON.deserialize(res.getBody(),MemberWrapper.class);
                system.debug('mailChimpList--->'+mailChimpList);
            }
             // iterate all the members.
            for(MemberWrapper.members memberMailchimpdata : mailChimpList.members) {
                Member__c memb = new Member__c();
                memb.MemberId__c = memberMailchimpdata.Id;
                memb.Language__c = memberMailchimpdata.language;
                memb.Email_Address__c = memberMailchimpdata.email_address;
                memb.Preferred__c = memberMailchimpdata.email_type;
                memb.Last_Updated__c = Date.valueOf(memberMailchimpdata.last_changed);
                memb.First_Name__c = memberMailchimpdata.merge_fields.FNAME;
                memb.Last_Name__c = memberMailchimpdata.merge_fields.LNAME;
                memb.Phone_Number__c = memberMailchimpdata.merge_fields.PHONE;
                //memb.Birthday__c = memberMailchimpdata.merge_fields.BIRTHDAY;
                memb.Address__c += memberMailchimpdata.merge_fields.ADDRESS.addr1+''+memberMailchimpdata.merge_fields.ADDRESS.addr2+''+memberMailchimpdata.merge_fields.ADDRESS.city
                    +''+memberMailchimpdata.merge_fields.ADDRESS.state+''+memberMailchimpdata.merge_fields.ADDRESS.country;
                
                
                memberList.add(memb);
            }
            
            try{
                if(!memberList.isEmpty()){ 
                    Schema.SObjectField f = Member__c.Fields.MemberId__c;
                    Database.upsert(memberList, f) ;
                }
            }
            catch(Exception e){
                System.debug('Error in upsert '+e.getMessage());
            }
        }
        
    } 

}