/**********************************************************************
* Name:     MemberWrapper
* Author:   BFL
*  
* ======================================================
* ======================================================
* Purpose:  Wrapper Class to get members data.
* ======================================================
* ======================================================
* History:                                                            
* VERSION   DATE            INITIALS      DESCRIPTION/FEATURES ADDED
* 1.0       16-Apr-2018     BFL           Wrapper Class to get members data.           
*   
***********************************************************************/

public class MemberWrapper {
    public String list_id{get;set;}
    public Integer total_items{get;set;}
    public list<links> links{get;set;}
    public list<members> members{get;set;}
    
    public class stats{
        public Integer avg_click_rate{get;set;}
        public Integer avg_open_rate{get;set;}
    }
    
    public class merge_fields{
        public String PHONE{get;set;}
        public String LNAME{get;set;}
        public ADDRESS ADDRESS{get;set;}
        public String FNAME{get;set;}
    }
    
    public class members{
        public String language{get;set;}
        public String ip_opt{get;set;}
        public String timestamp_signup{get;set;}
        public String timestamp_opt{get;set;}
        public String ip_signup{get;set;}
        public Integer member_rating{get;set;}
        public stats stats{get;set;}
        public String last_changed{get;set;}
        public merge_fields merge_fields{get;set;}
        public Boolean vip{get;set;}
        public String status{get;set;}
        public String email_client{get;set;}
        public String email_type{get;set;}
        public location location{get;set;}
        public String unique_email_id{get;set;}
        public String list_id{get;set;}
        public String email_address{get;set;}
        public list<links> links{get;set;}
        public String id{get;set;}
    }
    
    public class location{
        public Integer latitude{get;set;}
        public Integer gmtoff{get;set;}
        public Integer dstoff{get;set;}
        public Integer longitude{get;set;}
        public String country_code{get;set;}
        public String timezone{get;set;}
    }
    public class ADDRESS{
        public String country{get;set;}
        public String city{get;set;}
        public String state{get;set;}
        public String addr2{get;set;}
        public String zip{get;set;}
        public String addr1{get;set;}
    }
    public class links{
        public String href{get;set;}
        public String rel{get;set;}
        public String method{get;set;}
        public String targetSchema{get;set;}
        public String schema{get;set;}
        
    }
    
}